def fizzbuzz(value)
    # 再帰的に呼び出す
    if(value > 1)
        fizzbuzz(value-1)
    end

    if (value % 3 == 0 && value % 5 == 0) then
        puts("FizzBuzz ")
    elsif (value % 3 == 0) then
        puts("Fizz ")
    elsif (value % 5 == 0) then
        puts("Buzz ")
    else
        puts(value)
    end
end

# fizzbuzz関数呼び出し
fizzbuzz(500)